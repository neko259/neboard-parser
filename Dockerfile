FROM python:3-slim
COPY requirements.txt parser.py /
COPY parsers/ /parsers
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 8000
ENTRYPOINT ["python", "parser.py"]