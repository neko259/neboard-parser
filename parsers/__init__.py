class Parser:
    def preparse(self, raw_text, post_mapping):
        pass

    def parse(self, raw_text, post_mapping):
        pass

    def reflinks(self, raw_text):
        pass

    def tags(self):
        pass

    def line_breaks(self):
        pass

    name = None


def get_reflink(post_mapping, post_id):
    result = None
    if post_mapping is not None and post_id in post_mapping:
        post_data = post_mapping[post_id]
        result = '<a href={}>&gt;&gt;{}</a>'.format(post_data['url'], post_id)
        if post_data['opening']:
            result = '<b>' + result + '</b>'
    return result
