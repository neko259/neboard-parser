from parsers import Parser


class SimpleParser(Parser):
    name = 'simple'

    def preparse(self, raw_text, post_mapping):
        return raw_text

    def parse(self, raw_text, post_mapping):
        return raw_text.replace('\n', '<div class="br"></div>')

    def reflinks(self, raw_text):
        return []

    def tags(self):
        return []

    def line_breaks(self):
        return 1
