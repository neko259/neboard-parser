from xml import etree

import re
import bbcode

from urllib.parse import unquote
from parsers import Parser

__author__ = 'neko259'

from parsers import get_reflink

REFLINK_PATTERN = re.compile(r'^\d+$')
MULTI_NEWLINES_PATTERN = re.compile(r'(\r?\n){2,}')
ONE_NEWLINE = '\n'
REGEX_URL = re.compile(r'https?\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(/\S*)?')
LINE_BREAK_HTML = '<div class="br"></div>'
REGEX_ANY_LINE_BREAK = re.compile(r'(' + LINE_BREAK_HTML + '|\r|\n)')
QUOTE_REPLACEMENT = '\g<1>&gt;'


class TextFormatter:
    """
    An interface for formatter that can be used in the text format panel
    """

    def __init__(self):
        pass

    name = ''

    # Left and right tags for the button preview
    preview_left = ''
    preview_right = ''

    # Tag name to enclose the text in the form
    tag_name = ''

    has_input = 'false'
    input_prompt = ''

    def needs_input(self):
        return self.has_input == 'true'

    def get_definition(self):
        definition = {
            'name': self.name,
            'previewLeft': self.preview_left,
            'previewRight': self.preview_right,
            'tagLeft': ('[' + self.tag_name + '=$input]') if self.needs_input() else ('[' + self.tag_name + ']'),
            'tagRight': '[/' + self.tag_name + ']',
            'needsInput': self.has_input,
        }
        if self.needs_input():
            definition['inputHint'] = self.input_prompt
        return definition


class AutolinkPattern:
    def handleMatch(self, m):
        link_element = etree.Element('a')
        href = m.group(2)
        link_element.set('href', href)
        link_element.text = href

        return link_element


class QuotePattern(TextFormatter):
    name = '>q'
    preview_left = '<span class="quote">'
    preview_right = '</span>'

    tag_name = 'quote'


class SpoilerPattern(TextFormatter):
    name = 'spoiler'
    preview_left = '<span class="spoiler">'
    preview_right = '</span>'

    tag_name = 'spoiler'


class CommentPattern(TextFormatter):
    name = 'c'
    preview_left = '<span class="comment">// '
    preview_right = '</span>'

    tag_name = 'comment'


# TODO Use <s> tag here
class StrikeThroughPattern(TextFormatter):
    name = 's'
    preview_left = '<span class="strikethrough">'
    preview_right = '</span>'

    tag_name = 's'


class ItalicPattern(TextFormatter):
    name = 'i'
    preview_left = '<i>'
    preview_right = '</i>'

    tag_name = 'i'


class BoldPattern(TextFormatter):
    name = 'b'
    preview_left = '<b>'
    preview_right = '</b>'

    tag_name = 'b'


class CodePattern(TextFormatter):
    name = 'code'
    preview_left = '<code>'
    preview_right = '</code>'

    tag_name = 'code'


class HintPattern(TextFormatter):
    name = 'hint'
    preview_left = '<span class="hint">'
    preview_right = '</span>'

    tag_name = 'hint'

    has_input = 'true'
    input_prompt = 'Enter tooltip text'


class ColorPattern(TextFormatter):
    name = 'color'
    preview_left = '<span style="color:yellow;">'
    preview_right = '</span>'

    tag_name = 'color'

    has_input = 'true'
    input_prompt = 'Enter html-style color (e.g. #ccc or yellow)'


def render_reflink(tag_name, value, options, parent, context):
    result = '>>%s' % value

    post_mapping = context.get('post_mapping')
    reflinks = context.get('reflinks')

    if REFLINK_PATTERN.match(value):
        post_id = int(value)

        if reflinks is not None and post_id not in reflinks:
            reflinks.append(post_id)
        else:
            reflink = get_reflink(post_mapping, value)
            if reflink:
                result = reflink

    return result


def render_quote(tag_name, value, options, parent, context):
    source = options.get('quote') or options.get('source')

    if source:
        result = '<div class="multiquote"><div class="quote-header">%s</div><div class="quote-text">%s</div></div>' % (source, value)
    else:
        # Insert a ">" at the start of every line
        result = '<span class="quote">&gt;{}</span>'.format(
            REGEX_ANY_LINE_BREAK.sub(QUOTE_REPLACEMENT, value))

    return result


def render_hint(tag_name, value, options, parent, context):
    if 'hint' in options:
        hint = options['hint']
        result = '<span class="hint" title="{}">{}</span>'.format(hint, value)
    else:
        result = value
    return result


def render_spoiler(tag_name, value, options, parent, context):
    return '<span class="spoiler">{}</span>'.format(value)


formatters = [
    QuotePattern(),
    SpoilerPattern(),
    ItalicPattern(),
    BoldPattern(),
    CommentPattern(),
    StrikeThroughPattern(),
    CodePattern(),
    HintPattern(),
    ColorPattern(),
]


PREPARSE_PATTERNS = {
    r'(?<!>)>>(\d+)': r'[post]\1[/post]',
    r'^>([^>].+)': r'[quote]\1[/quote]',
    r'^//\s?(.+)': r'[comment]\1[/comment]',
}


TAG_DEFS = []
for formatter in formatters:
    TAG_DEFS.append(formatter.get_definition())


class InternalParser:
    def __init__(self):
        # The newline hack is added because br's margin does not work in all
        # browsers except firefox, when the div's does.
        self.parser = bbcode.Parser(newline=LINE_BREAK_HTML)

        self.parser.add_formatter('post', render_reflink, strip=True)
        self.parser.add_formatter('quote', render_quote, strip=True)
        self.parser.add_formatter('hint', render_hint, strip=True)
        self.parser.add_formatter('spoiler', render_spoiler, strip=True)
        self.parser.add_simple_formatter(
            'comment', '<span class="comment">// %(value)s</span>', strip=True)
        self.parser.add_simple_formatter(
            's', '<span class="strikethrough">%(value)s</span>')
        self.parser.add_simple_formatter('code',
                                         '<pre><code>%(value)s</pre></code>',
                                         render_embedded=False,
                                         escape_html=True,
                                         replace_links=False,
                                         replace_cosmetic=False)

        self.reflink_parser = bbcode.Parser()
        self.reflink_parser.add_formatter('post', render_reflink, strip=True)

    def preparse(self, text, post_mapping):
        """
        Performs manual parsing before the bbcode parser is used.
        Preparsed text is saved as raw and the text before preparsing is lost.
        """
        new_text = MULTI_NEWLINES_PATTERN.sub(ONE_NEWLINE, text)

        for key, value in PREPARSE_PATTERNS.items():
            new_text = re.sub(key, value, new_text, flags=re.MULTILINE)

        for link in REGEX_URL.findall(text):
            new_text = new_text.replace(link, unquote(link))

        return new_text

    def parse(self, text, post_mapping):
        return self.parser.format(text, post_mapping=post_mapping)

    def get_reflinks(self, text):
        reflinks = []
        self.reflink_parser.format(text, reflinks=reflinks)
        return reflinks


parser = InternalParser()


class BbCodeParser(Parser):
    name = 'bbcode'

    def __init__(self) -> None:
        super().__init__()
        self.parser = parser

    def preparse(self, raw_text, post_mapping):
        return self.parser.preparse(raw_text, post_mapping)

    def parse(self, raw_text, post_mapping):
        return self.parser.parse(raw_text, post_mapping)

    def reflinks(self, raw_text):
        return self.parser.get_reflinks(raw_text)

    def tags(self):
        return TAG_DEFS

    def line_breaks(self):
        return 1
