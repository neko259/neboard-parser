import re

import markdown
from markdown.inlinepatterns import Pattern
from markdown.util import etree


__author__ = 'neko259'

from parsers import Parser, get_reflink

AUTOLINK_PATTERN = r'(https?://\S+)'
QUOTE_PATTERN = r'^(?<!>)(>[^>].*)$'
REFLINK_PATTERN = r'((>>)(\d+))'
SPOILER_PATTERN = r'%%([^(%%)]+)%%'
COMMENT_PATTERN = r'^(//(.+))'
STRIKETHROUGH_PATTERN = r'~(.+)~'
DASH_PATTERN = r'--'


class TextFormatter:
    """
    An interface for formatter that can be used in the text format panel
    """

    def __init__(self):
        pass

    name = ''

    # Left and right tags for the button preview
    preview_left = ''
    preview_right = ''

    # Left and right characters for the textarea input
    format_left = ''
    format_right = ''

    def get_definition(self):
        definition = {
            'name': self.name,
            'previewLeft': self.preview_left,
            'previewRight': self.preview_right,
            'tagLeft': self.format_left,
            'tagRight': self.format_right,
            'needsInput': 'false',
        }
        return definition


class AutolinkPattern(Pattern):
    def handleMatch(self, m):
        link_element = etree.Element('a')
        href = m.group(2)
        link_element.set('href', href)
        link_element.text = href

        return link_element


class QuotePattern(Pattern, TextFormatter):
    name = 'q'
    preview_left = '<span class="quote">&gt; '
    preview_right = '</span>'

    format_left = '&gt;'

    def handleMatch(self, m):
        quote_element = etree.Element('span')
        quote_element.set('class', 'quote')
        quote_element.text = m.group(2)

        return quote_element


class ReflinkPattern(Pattern):
    def handleMatch(self, m):
        post_id = m.group(4)

        post_mapping = self.md.post_mapping
        if post_mapping and post_id in post_mapping:
            mapping = post_mapping[post_id]

            if mapping['opening']:
                top_element = etree.Element('b')
                ref_element = etree.SubElement(top_element, 'a')
            else:
                top_element = etree.Element('a')
                ref_element = top_element

            ref_element.set('href', mapping['url'])
            ref_element.text = markdown.util.AtomicString(m.group(2))

            return top_element


class SpoilerPattern(Pattern, TextFormatter):
    name = 'spoiler'
    preview_left = '<span class="spoiler">'
    preview_right = '</span>'

    format_left = '%%'
    format_right = '%%'

    def handleMatch(self, m):
        quote_element = etree.Element('span')
        quote_element.set('class', 'spoiler')
        quote_element.text = m.group(2)

        return quote_element


class CommentPattern(Pattern, TextFormatter):
    name = ''
    preview_left = '<span class="comment">// '
    preview_right = '</span>'

    format_left = '//'

    def handleMatch(self, m):
        quote_element = etree.Element('span')
        quote_element.set('class', 'comment')
        quote_element.text = '//' + m.group(3)

        return quote_element


class StrikeThroughPattern(Pattern, TextFormatter):
    name = 's'
    preview_left = '<span class="strikethrough">'
    preview_right = '</span>'

    format_left = '~'
    format_right = '~'

    def handleMatch(self, m):
        quote_element = etree.Element('span')
        quote_element.set('class', 'strikethrough')
        quote_element.text = m.group(2)

        return quote_element


class ItalicPattern(TextFormatter):
    name = 'i'
    preview_left = '<i>'
    preview_right = '</i>'

    format_left = '_'
    format_right = '_'


class BoldPattern(TextFormatter):
    name = 'b'
    preview_left = '<b>'
    preview_right = '</b>'

    format_left = '__'
    format_right = '__'


class CodePattern(TextFormatter):
    name = 'code'
    preview_left = '<code>'
    preview_right = '</code>'

    format_left = '    '


class DashPattern(Pattern):
    def handleMatch(self, m):
        return u'â€”'


class NeboardMarkdown(markdown.Extension):
    def extendMarkdown(self, md):
        self._add_neboard_patterns(md)
        self._delete_patterns(md)

    def _delete_patterns(self, md):
        del md.parser.blockprocessors['quote']

        del md.inlinePatterns['image_link']
        del md.inlinePatterns['image_reference']

    def _add_neboard_patterns(self, md):
        autolink = AutolinkPattern(AUTOLINK_PATTERN, md)
        quote = QuotePattern(QUOTE_PATTERN, md)
        reflink = ReflinkPattern(REFLINK_PATTERN, md)
        spoiler = SpoilerPattern(SPOILER_PATTERN, md)
        comment = CommentPattern(COMMENT_PATTERN, md)
        strikethrough = StrikeThroughPattern(STRIKETHROUGH_PATTERN, md)
        dash = DashPattern(DASH_PATTERN, md)

        md.inlinePatterns[u'autolink_ext'] = autolink
        md.inlinePatterns[u'spoiler'] = spoiler
        md.inlinePatterns[u'strikethrough'] = strikethrough
        md.inlinePatterns[u'comment'] = comment
        md.inlinePatterns[u'reflink'] = reflink
        md.inlinePatterns[u'quote'] = quote
        md.inlinePatterns[u'dash'] = dash


# def make_extension(configs=None):
#     return NeboardMarkdown(configs=configs)
#
#
# neboard_extension = make_extension()


formatters = [
    QuotePattern(''),
    SpoilerPattern(''),
    ItalicPattern(),
    BoldPattern(),
    CommentPattern(''),
    StrikeThroughPattern(''),
    CodePattern(),
]


class MarkdownParser(Parser):
    name = 'markdown'

    def __init__(self) -> None:
        super().__init__()
        self.md = markdown.Markdown(extensions=[NeboardMarkdown()],
                                    safe_mode='escape')

    def preparse(self, raw_text, post_mapping):
        return raw_text

    def parse(self, raw_text, post_mapping):
        self._reset()
        self.md.post_mapping = post_mapping
        return self.md.convert(raw_text)

    def reflinks(self, raw_text):
        return [pattern[2] for pattern in re.findall(REFLINK_PATTERN, raw_text)]

    def tags(self):
        return [formatter.get_definition() for formatter in formatters]

    def _reset(self):
        self.md.reset()
        self.md.post_mapping = None

    def line_breaks(self):
        return 2
