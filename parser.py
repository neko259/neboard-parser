import argparse
import datetime
import json
import os

import waitress
from flask import Flask, request, jsonify

from parsers import Parser

from parsers.bbcode_neboard import BbCodeParser
from parsers.markdown_neboard import MarkdownParser
from parsers.simple import SimpleParser


ATTR_RAW_TEXT = 'raw_text'
ATTR_POST_MAPPING = 'post_mapping'


app = Flask(__name__)

last_update = str(datetime.datetime.now())


def wrap_result(data):
    return jsonify(data)


PARSERS = {}
for parser_cls in Parser.__subclasses__():
    PARSERS[parser_cls.name] = parser_cls()


@app.route('/', methods=['GET'])
def list_methods():
    """List available parser methods"""
    methods = []
    for method in PARSERS.keys():
        methods.append(
            {
                'name': method,
                'lineBreaks': PARSERS[method].line_breaks()
            })
    return wrap_result({'methods': methods})


@app.route('/<method>/reflinks/', methods=['POST'])
def get_reflinks(method):
    data = request.form
    raw_text = data.get(ATTR_RAW_TEXT)

    return wrap_result({'reflinks': PARSERS[method].reflinks(raw_text)})


@app.route('/<method>/preparse/', methods=['POST'])
def preparse(method):
    data = request.form
    raw_text = data.get(ATTR_RAW_TEXT)
    post_mapping = data.get(ATTR_POST_MAPPING)

    return wrap_result({'text': PARSERS[method].preparse(raw_text, post_mapping)})


@app.route('/<method>/parse/', methods=['POST'])
def parse(method):
    data = request.form
    raw_text = data.get(ATTR_RAW_TEXT)
    post_mapping = json.loads(data.get(ATTR_POST_MAPPING))

    return wrap_result({'text': PARSERS[method].parse(raw_text, post_mapping)})


@app.route('/<method>/tags/', methods=['GET'])
def get_tags(method):
    return wrap_result({'tags': PARSERS[method].tags()})


@app.route('/health', methods=["GET"])
def health():
    return '', 204


@app.route('/last-update')
def get_last_update():
    return wrap_result({'time': last_update})


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Neboard parser service')
    parser.add_argument('--port', type=int, help='Port to run on', default=8000)

    args = parser.parse_args()

    threads = os.environ.get('THREADS')
    if threads:
        threads = int(threads)
    else:
        threads = 4

    waitress.serve(app, port=args.port)
